package jtm.activity05;

import jtm.activity04.Transport;
import jtm.activity04.Road;

public class Ship extends Transport {

    protected byte number_of_sails;

    public Ship(String id, byte Sails) {
        super(id, 0, 0);
        this.number_of_sails = Sails;
    }

    public Ship(String id, float consumption, int tankSize) {
        super(id, consumption, tankSize);
    }

    @Override
    public String move(Road road) {
        if(!(road instanceof WaterRoad)) {
            return "Cannot sail on " + road.toString();
        }
        return this.getId() + " Ship is sailing on " + road.toString() + " with " + this.number_of_sails + " sails";
    }
}
