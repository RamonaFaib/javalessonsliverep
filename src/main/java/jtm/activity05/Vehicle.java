package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Vehicle extends Transport {

    protected int number_of_wheels;

    public Vehicle(String id, float consumption, int tankSize,int wheels) {
        super(id, consumption, tankSize);
        
        this.number_of_wheels = wheels;
    }

        @Override
        public String move(Road road) {
            if(!(road instanceof Road)) {
                return "Cannot drive on " + road.toString();
            }
            return this.getId() + " Vehicle is driving on " + road.toString() + " with " + number_of_wheels + " wheels";
    }
}
