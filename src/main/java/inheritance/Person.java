package inheritance;

public class Person {

    protected enum Gender {
        MALE, FEMALE, UNDEFINED
    }
    protected String name;
    protected int birthDate;
    protected String gender;
}

class Employee extends Person {
    protected double salary;
    protected int experience;

    public String getDetails() {
        String ref = "_";
        String ref2 = "_";

        switch (gender) {
            case "MALE":
                ref = "he";
                break;
            case "FEMALE":
                ref = "she";
                break;
            case "UNDEFINED":
                ref = "_";
                break;
        }

        return "The Employee " + this.name + ", is born on " + this.birthDate + ", " + ref + "salary is " + this.salary +
                "$ " + ref2 + "has " + this.experience + " years of experience";
    }
}

class Manager extends Employee {
    protected String department;
}