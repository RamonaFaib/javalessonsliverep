package examples.abstraction;

final class A {

    protected void theVoid() {

    }

}

public class FinalTest extends A {
    // final String var;

    public FinalTest() {
        // this.var = var;

    }

    @Override
    protected void theVoid() {
        super.theVoid();
    }
}

class B extends FinalTest{
    public void main() {
        this.theVoid();

    }

}