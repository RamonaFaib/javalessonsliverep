package examples;

import org.junit.Test;

public class TestRef  implements Cloneable{

    private int a = 0;
    private int b= 0;

    private TestRef(int a, int b) {
        this.a = a;
        this.b = b;
        this.passThis(this);
        TestRef instance = this;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        TestRef instance1 = new TestRef(1,2);
     //   TestRef instance2 = (TestRef) instance1.clone();
        // instance2.a = 3;

       // System.out.println("instance1 a = " + instance1.a + " instance1 b = " + instance1.b);
       // System.out.println("instance2 a = " + instance2.a + " instance2 b = " + instance2.b);
    }

    private void passThis(Object object) {
        TestRef instance = (TestRef) object;
        System.out.println(instance.a);

    }
}
