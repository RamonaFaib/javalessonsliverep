package examples;

public class ForStaticConstr {
    public static void main(String[] args) {
        TestContrStat instance = new TestContrStat();
       // instance.callInst();

       // TestContrStat.call();
        // TestContrStat.call2();
    }
}

class TestContrStat{
    static {
        System.out.println("Static constructor is called!");
    }

    static void call() {
        System.out.println("The method is called!");
    }

    static void call2() {
        System.out.println("The 2nd method is called!");
    }

    public void callInst() {
        System.out.println("The instance method is called!");
    }
}
