package examples;

public class TestConstructor {

    private int a = 0;

    static {
        System.out.println("Static constructor!");
    }

    private TestConstructor() {

    }

    public static void main(String[] args) {
        TestConstructor instance = new TestConstructor();
        System.out.println("Constructor with no parameters called!");
    }

    public static TestConstructor getInstance() {
        return new TestConstructor();

    }
}
class TestConstructor2 {

    public static void main(String[] args) {
        TestConstructor2 instance = new TestConstructor2();
    }

}
