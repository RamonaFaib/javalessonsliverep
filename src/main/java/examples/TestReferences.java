package examples;

public class TestReferences {
    int a = 0;
    int b = 0;
    String text = "abcd";

    private TestReferences(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public static void main(String[] args) {
    TestReferences object1 = new TestReferences(23, 48);
    TestReferences object2 = object1;
    System.out.println(object1.a);
    object2.a = 36;
    System.out.println(object1.a);

    }
}
