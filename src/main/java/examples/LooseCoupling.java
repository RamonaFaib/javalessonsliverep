package examples;

public class LooseCoupling {

    private String name;

    public String getName() {
        if(this.name != null) {
            return this.name;
        }
        return "not initialized";
    }
    public void  setName(String s) {
        if (s == null) {
            System.out.println("You can't initialize the name to null");
        }
    }
}

    class C {
    public static void main(String[] args) {

        LooseCoupling ob = new LooseCoupling();
        ob.setName(null);
        System.out.println("Name is " + ob.getName());
    }
}
