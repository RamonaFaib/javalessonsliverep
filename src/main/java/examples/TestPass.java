package examples;

public class TestPass {

    private int a = 0;

    public static void main(String[] args) {
        int a = 2;

        TestPass instance = new TestPass();
        instance.passParameters(a);
        System.out.println(a);
    }

    private void passParameters(int a) {
        a = 1;

    }

}
