package examples;

public class StructuredCode {

   static int account_number = 20;
    static int account_balance = 100;

    public static void main(String[] args) {

        account_balance = account_balance + 100;
        StructuredCode.showData();

        //System.out.println("Account Number = " + account_number);
        //System.out.println("Account Balance = " + account_balance);

        account_balance = account_balance + 50;
        StructuredCode.showData();
        //System.out.println("Account Number = " + account_number);
        //System.out.println("Account Balance = " + account_balance);

        account_balance = account_balance + 10;
        StructuredCode.showData();

        //System.out.println("Account Number = " + account_number);
        //System.out.println("Account Balance = " + account_balance);

    }

    private static void showData() {
        System.out.println("Account Number = " + StructuredCode.account_number);
        System.out.println("Account Balance = " + StructuredCode.account_balance);
    }
}