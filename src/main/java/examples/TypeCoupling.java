package examples;

public class TypeCoupling {

    public String name;

    public String getName() {
        if(this.name != null) {
            return this.name;
        }
            return "not initialized";
    }
    public void  setName(String s) {
        if (s == null) {
            System.out.println("You can't initialize the name to null");
        }
    }
}

class B {
    public static void main(String[] args) {

        TypeCoupling ob = new TypeCoupling();
        ob.name = null;
        System.out.println("Name is " + ob.name);
    }
}