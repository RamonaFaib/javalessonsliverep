package examples;

public class Display {

    public static void main(String[] args) {

        Multiplication m = new Multiplication();
        System.out.println(m.mul(2, 7));

    }
}

class Multiplication {

    private int a = 3;
    private int b = 4;

    public int mul(int a, int b) {
        this.a = a;
        this.b = b;
        return a * b;
    }
}