package examples;

import java.lang.Math;

public class dimensions {

    int attribute;

    public static void main(String[] args) {

        // first array
        int elements[] = {1, 2, 3, 4, 5, 6};

        // second, larger one
        int hold[] = {10, 9, 8, 7, 6, 5, 4, 3, 2,1};

        // copy all emen for one to another
        // System.arraycopy();

        // we can't access att because there is no object

        dimensions myobject = new dimensions();
        myobject.attribute = 1;

        System.out.println(java.lang.Math.ceil(13.4));
        System.out.println(java.lang.Math.sqrt(64));
        System.out.println(java.lang.Math.random());

        String myString = "Interesting";
        String myString2 = "Interestin";
        myString2 += "g";

        System.out.println(myString.equals(myString2));

        // multi line
        String someString = "This is\n" + "some\n" + "string\n";
        System.out.println(someString);

        System.out.println(someString.replace("T", "t"));


    }
}
